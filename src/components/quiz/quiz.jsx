import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { push } from 'connected-react-router';

import { answerQuestion } from '../../sagas/quiz';

import { Question } from './question';

class QuizComponent extends React.Component {
  constructor() {
    super();
    this.answerQuestion = this.answerQuestion.bind(this);
  }

  answerQuestion(answer) {
    const questionNumber = parseInt(this.props.match.params.question, 10);
    this.props.answerQuestion(questionNumber, answer);
    if (questionNumber < this.props.questions.length) {
      this.props.nextQuestion(questionNumber + 1);
    } else {
      this.props.finishQuiz();
    }
  }

  render() {
    const questionIndex = parseInt(this.props.match.params.question, 10) - 1;
    const { category, question } = this.props.questions[questionIndex];

    return (
      <div className="quiz">
        <Question category={category} question={question} answerQuestion={this.answerQuestion} />

        <p className="progress">
          {questionIndex + 1} / {this.props.questions.length}
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    questions: state.quiz.quizQuestions,
    state: state,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    answerQuestion: (question, answer) => {
      dispatch(answerQuestion(question, answer));
    },
    nextQuestion: questionNumber => {
      dispatch(push(`/quiz/${questionNumber}`));
    },
    finishQuiz: () => {
      dispatch(push('/results'));
    },
  };
};

export const Quiz = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(QuizComponent)
);
