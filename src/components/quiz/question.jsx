import * as React from 'react';

import decodeEntities from '../../utils/decode-entities';

import './question.scss';

export const Question = ({ category, question, answerQuestion }) => {
  const clickTrue = () => {
    answerQuestion('True');
  };

  const clickFalse = () => {
    answerQuestion('False');
  };

  return (
    <div className="question">
      <h1>{category}</h1>

      <h2>{decodeEntities(question)}</h2>

      <p className="response">
        <button className="btn true" onClick={clickTrue}>
          True
        </button>
        <button className="btn false" onClick={clickFalse}>
          False
        </button>
      </p>
    </div>
  );
};
