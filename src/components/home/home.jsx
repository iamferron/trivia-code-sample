import * as React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import Loader from 'react-loader-spinner';

import { fetchQuiz } from '../../sagas/quiz';

class HomeComponent extends React.Component {
  constructor() {
    super();
    this.fetchQuiz = this.fetchQuiz.bind(this);
  }

  componentDidMount() {
    this.fetchQuiz();
  }

  fetchQuiz() {
    this.props.fetchQuiz();
  }

  render() {
    if (
      this.props.isLoading === true ||
      (this.props.questions === null && this.props.error === null)
    ) {
      return (
        <div className="home">
          <Loader type="TailSpin" color="#3cf" />
          <p>Loading Quiz</p>
        </div>
      );
    } else if (this.props.error !== null) {
      return (
        <div className="home">
          <h1 className="error">Error Loading Quiz</h1>
          <p>
            <button onClick={this.fetchQuiz}>Try Again</button>
          </p>
        </div>
      );
    } else {
      return (
        <div className="home">
          <h1>Welcome to the Trivia Challenge!</h1>

          <p>You will be presented with {this.props.questions.length} True or False questions.</p>

          <p>Can you score 100%?</p>

          <p>
            <Link className="btn" to="/quiz/1">
              Begin
            </Link>
          </p>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    questions: state.quiz.quizQuestions,
    isLoading: state.quiz.quizIsLoading,
    error: state.quiz.quizError,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchQuiz: () => dispatch(fetchQuiz()),
  };
};

export const Home = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(HomeComponent)
);
