import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { resetAnswers } from '../../sagas/quiz';

import { Result } from './result';

class ResultsComponent extends React.Component {
  componentWillUnmount() {
    this.props.resetAnswers();
  }

  render() {
    const { questions, answers } = this.props;

    const results = questions.map((question, i) => (
      <Result
        key={i}
        question={question.question}
        isCorrect={question.correct_answer === answers[i]}
      />
    ));

    const correctAnswers = questions.reduce((correctAnswers, question, i) => {
      return answers[i] === question.correct_answer ? correctAnswers + 1 : correctAnswers;
    }, 0);

    return (
      <div className="results">
        <h1>
          You scored {correctAnswers} / {questions.length}
        </h1>

        <div className="questions">{results}</div>

        <p>
          <Link className="btn" to="/">
            Play Again?
          </Link>
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    questions: state.quiz.quizQuestions,
    answers: state.quiz.quizAnswers,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetAnswers: () => {
      dispatch(resetAnswers());
    },
  };
};

export const Results = connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultsComponent);
