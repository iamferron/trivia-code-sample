import * as React from 'react';

export default function decodeEntities(string) {
  // eslint-disable-next-line react/no-danger
  return <span dangerouslySetInnerHTML={{ __html: string }} />;
}
