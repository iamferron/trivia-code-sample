# Code Sample: Open Trivia Database

### Scripts

`npm start`: Start the Webpack Dev Server

`npm run start:prod`: Build for Production and Start Express

`npm test`: Run Jest

`npm run lint`: Lint

`npm run prettier`: Prettier

`npm run build`: Build for production

### Tech stack:

- React 16.4.1
- Redux-Saga 0.16.0
- Express 4.16.3
- Babel 6.26.3
- Webpack 4.16.2
- Jest 23.4.1
- ESLint 4.19.1
- Prettier 1.13.7

### Development times:

- Configuration: 1 hour
- React Development: 3 hours
